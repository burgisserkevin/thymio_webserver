# -*- coding: utf-8 -*-

from flask import Flask, jsonify, render_template, request, redirect, url_for, make_response, session, json
import subprocess
import random

# create Flask app
app = Flask(__name__)

@app.route('/')
def index():
	return render_template('index.html')

# edit distance
@app.route('/distance', methods=['POST'])
def handle_distance():
	distance = request.form['inputDistance']
	print("DISTANCE VALUE: " + distance)
	speed = request.form['inputSpeed']
	print("SPEED VALUE: " + speed)
	subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from moveByDistance import *; moveByDistance1("+speed+","+distance+")'\"", shell=True)
	return render_template('index.html')

@app.route('/rotation', methods=['POST'])
def handle_rotation():
	rotation = request.form['inputRotation']
	subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from rotate import *; rotate(" + rotation + ")'\"", shell=True)
	#return redirect(url_for('index'))
	return render_template('index.html')

@app.route('/forward', methods=['POST'])
def handle_move_forward():
	subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from basicMoves import *; forward()'\"", shell=True)
	return render_template('index.html')

@app.route('/left', methods=['POST'])
def handle_move_left():
	subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from basicMoves import *; left()'\"", shell=True)
	return render_template('index.html')

@app.route('/right', methods=['POST'])
def handle_move_right():
	subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from basicMoves import *; right()'\"", shell=True)
	return render_template('index.html')

@app.route('/bottom', methods=['POST'])
def handle_move_backward():
	subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from basicMoves import *; backward()'\"", shell=True)
	return render_template('index.html')

@app.route('/stop', methods=['POST'])
def handle_stop():

	subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from basicMoves import *; stop()'\"", shell=True)
	return render_template('index.html')

@app.route('/follow-line', methods=['POST'])
def handle_follow_line():
	subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from followLine import *; followLine(1)'\"", shell=True)
	return render_template('index.html')

@app.route('/follow_canyon', methods=['POST'])
def handle_follow_canyon():
	subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from followLine import *; followLine(2)'\"", shell=True)
	return render_template('index.html')

@app.route('/follow_bridge', methods=['POST'])
def handle_follow_bridge():
	subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from crossBridge import *; crossBridge()'\"", shell=True)
	return render_template('index.html')

@app.route('/find-line', methods=['POST'])
def handle_find_line():
	subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from findLine import *; findLine()'\"", shell=True)
	return render_template('index.html')

@app.route('/push-object', methods=['POST'])
def handle_push_object():
	subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from pushWallUntilLine import *; pushWallUntilLine1()'\"", shell=True)
	return render_template('index.html')

@app.route('/drilling', methods=['POST'])
def handle_drilling():
	subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from forage import *; forage()'\"", shell=True)
	return render_template('index.html')

@app.route('/bypass-object', methods=['POST'])
def handle_bypass_object():
	subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from bypassWall import *; bypassWall()'\"", shell=True)
	return render_template('index.html')

@app.route('/play_music', methods=['POST'])
def handle_play_music():
	subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from playSound import *; playSound()'\"", shell=True)
	return render_template('index.html')

@app.route('/reset-sensors', methods=['POST'])
def handle_reset_sensors():
	subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from resetRobot import *; resetRobot()'\"", shell=True)
	return render_template('index.html')

@app.route('/leds_controller', methods=['POST'])
def handle_leds_controller():
	if request.form.get('switch_led_input'):
		subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from ledsOnOff import *; ledsOnOff1(True)'\"", shell=True)
		return render_template('index.html')
	else:
		print('LEDS OFF')
		subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from ledsOnOff import *; ledsOnOff1(False)'\"", shell=True)
		return render_template('index.html')

@app.route('/disco_mode', methods=['POST'])
def handle_disco_mode():
	subprocess.Popen("sshpass -p tic2017 ssh -o StrictHostKeyChecking=no projettic@192.168.100.201 \"cd /home/projettic/Desktop/Project && python3 -c 'from disco import *; disco1()'\"", shell=True)
	return render_template('index.html')

############################################################
#Functions to get the state of the robot
############################################################
@app.route('/getActualData', methods=['GET', 'POST'])
def getActualData():
	dicsss = [{'1' : '111'}]
	data = request.json
	jsonStr = json.dumps(data)
	data = ""

	with open("/home/thymio/Desktop/data.txt", 'r') as fo:
		fo.seek(0)
		data = fo.read()
		fo.close()

	json_acceptable_string = data.replace("'", "\"")
	d = json.loads(json_acceptable_string)

	return jsonify(d)

@app.route('/getOldData', methods=['GET', 'POST'])
def getOldData():
	dicsss = [{'1' : '111'}]
	data = request.json
	jsonStr = json.dumps(data)
	data = ""

	with open("/home/thymio/Desktop/oldData.txt", 'r') as fo:
		fo.seek(0)
		data = fo.read()
		fo.close()

	json_acceptable_string = data.replace("'", "\"")
	d = json.loads(json_acceptable_string)

	return jsonify(d)

@app.route('/writeData', methods=['GET', 'POST'])
def writeData():
	dicsss = [{'1' : '111'}]
	Olddata = ""

	with open("/home/thymio/Desktop/data.txt", 'r') as fo:
		print('file open')
		fo.seek(0)
		Olddata = fo.read()
		fo.close()


	with open("/home/thymio/Desktop/oldData.txt", 'w+') as fo:
		fo.seek(0)
		fo.write(Olddata)
		fo.close()
	
	data = request.json
	jsonStr = json.dumps(data)

	with open("/home/thymio/Desktop/data.txt", 'w+') as fo:
		fo.seek(0) #attention
		fo.write(jsonStr)
		fo.seek(0)
		data = fo.read()
		fo.close()
	
	return jsonify(dicsss)


############################################################
############################################################

###  test  ###
'''@app.route('/mkdir')
def mkdir():
	subprocess.Popen("sshpass -p 'tic2017' ssh -o StrictHostKeyChecking=no projettic@192.168.17.3 \"cd /home/projettic/Desktop/ && python3 -c 'from turnRight import *; turnRight(180)'\"", shell=True)
	return 'done'

# define function for root url and return Hello World
@app.route('/test')
def test():
	returnvalue = {'funcname': 'test{0}'.format(random.randrange(4)), 'values':[random.randrange(33),random.randrange(33),random.randrange(33)]}
	return render_template('test.html', returnvalue = returnvalue)

# define function for /nextfunc url
@app.route('/nextfunc')
def nextfunc():
	# create return value with a func name (between func0 and func3) with led color value (red, green and blue between 0 and 32)
	returnvalue = {'funcname': 'test{0}'.format(random.randrange(4)), 'values':[random.randrange(33),random.randrange(33),random.randrange(33)]}
	# return returnvalue object in json
	return jsonify(returnvalue)
'''

############################################################
############################################################

if __name__ == '__main__':
	app.run(debug=True)