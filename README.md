# Thymio_WebServer

## Note
This repository is part of my portfolio that aims to show projects that I have realised while being at the School of Engineering and Architecure of Fribourg (https://www.heia-fr.ch/en/).
Therefore that repository was not created to be directly used for deployment but rather to show what kind of work I have done during bachelor's degree courses.

## Context
1st year IT students at College of Engnieering of Fribourg must participate in the “ICT project”. 
This is an introduction project to robotics and network. That year the project was called Mission to March 2017 (M2M17). 
The aim of this project was to carry out a mission to the planet Mars with a Thymio II robot piloted from Earth. 
Since the two machines were more than 227,930 million kilometers apart from each other, each and every data transmission were delayed by 10s. In addition, 10% of the data were lost during the journey. 
Students had to imagine, implement and validate an infrastructure to meet this challenge.

At the end of the project, our robot was able to drive on the simulation track imagined by our teachers. The track was simulated by a large black line on white paper sheet. 
Multiple obstacles were added on the track (bridges, stones, other objects) and the robot had to push/follow/bypass them obstacles depending on the nature of obstacles.

## Thymio_WebServer
This repository contains the web-server part. It aims to display a control interface used to control the robot.
Using the web interface the user can order the robot to drive forward/backward or to the left/right, can modify robot’s speed or make it stop. The robot has numerous other features: he can find the line on the track, follow line, cross a bridge, push an object and bypass an object.
The web interface can also be used to make the robot play music and switch its leds on/off.

For more details about this project and illustrations of the interface, please look at [burgisser.me/project/thymio](https://burgisser.me/project/thymio/).

## Screenshots
Look at [demo](https://gitlab.com/burgisserkevin/thymio_webserver/-/tree/master/demo
)  
## Built With
* [Flask](https://flask.palletsprojects.com/en/1.1.x/) - Micro web framework written in Python
* [Bootstrap v4.0.0-beta.2](https://getbootstrap.com/docs/4.1/getting-started/introduction/) - CSS and JS parts

## Authors
* Completion date  : 20 February 2018
* **Kevin Bürgisser**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details